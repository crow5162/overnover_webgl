﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScoreBox : MonoBehaviour
{
    private bool decreaseScore = false;
    public int decreaseScoreCount = 3;

    private void OnTriggerStay2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            var btnActive = coll.gameObject.GetComponent<CharacterControll>().JumpBtnActive;

            //PC Controller Input
            if(Input.GetKey(KeyCode.Space) && !decreaseScore)
            {
                decreaseScore = true;
                GameManager.instance.DecreaseScore(decreaseScoreCount);
                GameManager.instance.CheckPlayerRank();
            }
            //Mobile Input
            else if(btnActive && !decreaseScore)
            {
                decreaseScore = true;
                GameManager.instance.DecreaseScore(decreaseScoreCount);
                GameManager.instance.CheckPlayerRank();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            if(decreaseScore)
            {
                decreaseScore = false;
            }
        }
    }
}
