﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJumpColl : MonoBehaviour
{
    private const string playerTag = "Player";
    public float jumpPower = 10.0f;

    //점프의 방식을 바꾸었다 
    //사운드처리만 첫 충돌 때 출력하기 
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == playerTag)
        {
            SoundManager.instance.StartSfx("JUMP");
        }
    }

    private void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.tag == playerTag)
        {
            var playerRbody = coll.gameObject.GetComponent<Rigidbody>();
            playerRbody.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
        }
    }
}
