﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement3D : MonoBehaviour
{
    public enum MoveDirectionMonster
    {
        MOVE_FRONT,
        MOVE_BACK
    }

    public float maxMoveTime;

    private float moveTime;
    private float moveSpeed = 3.0f;
    public MoveDirectionMonster firstDirec;

    private void Update()
    {
        moveTime += Time.deltaTime;

        if(firstDirec == MoveDirectionMonster.MOVE_BACK)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);

            if(moveTime < maxMoveTime)
            {
                transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            }
            else if (moveTime >= maxMoveTime)
            {
                firstDirec = MoveDirectionMonster.MOVE_FRONT;
                moveTime = 0f;
            }
        }
        else if (firstDirec == MoveDirectionMonster.MOVE_FRONT)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);

            if (moveTime < maxMoveTime)
            {
                transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            }
            else if (moveTime >= maxMoveTime)
            {
                firstDirec = MoveDirectionMonster.MOVE_BACK;
                moveTime = 0f;
            }
        }
    }
}
