﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControll3D : MonoBehaviour
{
    [Header("Character Controll")]
    public float moveSpeed;
    public float jumpForce;

    [Header("Character Status")]
    [SerializeField] private Vector3 respawnPoint;

    private bool isJump = false;
    private float backMoveSpeed;
    private bool onBackBtn = false;
    private float backTime;
    private float playerGroundCheckY;
    private CapsuleCollider capsulecoll;
    private float jumpCheckDist = 0.1f;

    private Rigidbody rBody;

    private void Start()
    {
        rBody = GetComponent<Rigidbody>();

        //Start 되었을 당시의 첫 지점을 리스폰 포인트로 잡습니다. 
        respawnPoint = transform.position;

        backMoveSpeed = moveSpeed;

        capsulecoll = GetComponent<CapsuleCollider>();
        playerGroundCheckY = (capsulecoll.bounds.size.y * 0.5f) + jumpCheckDist;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            backMoveSpeed = moveSpeed;
        }
    }

    private void FixedUpdate()
    {
        if (Scene3DManager.instance.gameState == Scene3DManager.GameState.INGAME)
        {
            if(Input.GetKey(KeyCode.DownArrow) || onBackBtn)
            {
                backMoveSpeed -= 0.1f;

                if(backMoveSpeed <= 0)
                {
                    backMoveSpeed = 0f;
                }

                rBody.MovePosition(transform.position + (Vector3.left * backMoveSpeed * Time.deltaTime));
            }
            else if (!Input.GetKey(KeyCode.DownArrow) || !onBackBtn)
            {
                rBody.MovePosition(transform.position + (Vector3.right * moveSpeed * Time.deltaTime));
            }

            if(Input.GetKey(KeyCode.Space) && !isJump)
            {
                rBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isJump = true;

                SoundManager.instance.StartSfx("JUMP");
            }
        }

        if(isJump)
        {
            RaycastHit rayHit;

            Debug.DrawRay(transform.position, Vector3.down * playerGroundCheckY, Color.red, 0.3f);

            if (Physics.Raycast(transform.position, Vector3.down, out rayHit, playerGroundCheckY))
            {
                isJump = false;
            }
        }
    }

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.layer == LayerMask.NameToLayer("DeadZone"))
        {
            transform.position = respawnPoint;
            SoundManager.instance.StartSfx("DEATH");
        }

        if(coll.gameObject.layer == LayerMask.NameToLayer("Coin"))
        {
            coll.gameObject.SetActive(false);
            SoundManager.instance.StartSfx("COIN");
        }
    }

    #region Mobile Input

    public void OnBackBtnDown()
    {
        onBackBtn = true;
    }

    public void OnBackBtnUp()
    {
        onBackBtn = false;
        backMoveSpeed = moveSpeed;
    }

    public void OnJumpBtnDown()
    {
        if(!isJump)
        {
            rBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isJump = true;

            SoundManager.instance.StartSfx("JUMP");
        }
    }

    #endregion  
}
