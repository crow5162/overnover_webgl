﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal3DScene : MonoBehaviour
{
    private const string player = "Player";
    public Transform nextPortal;

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.tag == player)
        {
            coll.gameObject.transform.position = nextPortal.position;
            SoundManager.instance.StartSfx("PORTAL");
        }
    }
}
