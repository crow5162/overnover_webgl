﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterEmotionController : MonoBehaviour
{
    public enum PlayerRank
    {
        RANK_A,
        RANK_B,
        RANK_C,
        RANK_D
    }

    [TextArea] public string[] BRankText;
    [TextArea] public string[] CRankText;
    [TextArea] public string[] DeadText;
    [TextArea] public string[] WaitText;

    [SerializeField] private bool isAngry;
    public PlayerRank playerRank;
    public float angryTime = 30.0f;
    private float idleTime = 0.0f;
    private Animator anim;
    public GameObject talkBubble;
    private Text talkText;
    private float rankBpauseTime;
    public float rankBTIme = 10.0f;             //플레이어의 랭크 B인 상황에서 이 시간마다 체크해서 확률로 멈춥니다
    [Range(0, 10)] public int stopChance = 5;   //랭크 B인 상태에서 멈출 확률, 10일 경우 무조건 멈춘다
    private CharacterControll charaControll;
    private CharacterStatus charaStatus;
    [SerializeField] private float findTime = 0.0f;
    [SerializeField] private Transform deadZoneTarget;
    private bool isResign = false;              //조작 불가 상태 (RANK_D)
    private bool rankChange = false;            //Rank 의 변동이 일어날 때 한번만 켜질 변수

    public bool IsAngry { get { return isAngry; } set { isAngry = value; } }
    public float FindTime { set { findTime = value; } }
    public bool IsResign { get { return isResign; } set { isResign = value; } }
    private const string deadZoneTag = "DeadZone";

    private void Start()
    {
        anim = GetComponent<Animator>();
        charaControll = GetComponent<CharacterControll>();
        charaStatus = GetComponent<CharacterStatus>();

        if(talkBubble != null)
        {
            talkText = talkBubble.GetComponentInChildren<Text>();
        }
        else
        {
            Debug.Log("Talk Bubble GameObject is NULL !!");
        }
    }

    private void Update()
    {
        #region SetWaitState

        if(!anim.GetBool("isMove") && !anim.GetBool("isJump") && !anim.GetBool("isClimb"))
        {
            idleTime += Time.deltaTime;
        }
        else
        {
            idleTime = 0;
        }

        #endregion

        #region WaitState

        if(angryTime <= idleTime)
        {
            //anim.SetTrigger("Angry");
            anim.SetBool("isAngry", true);

            if (!talkBubble.activeSelf)
            {
                if(playerRank == PlayerRank.RANK_A)
                {
                    talkBubble.SetActive(true);
                    talkText.text = WaitText[0];
                }
            }
        }
        else
        {
            anim.SetBool("isAngry", false);

            //가만히 대기하고 있는 상태에서 캐릭터의 말풍선이 활성화 된 상황 
            if (talkBubble.activeSelf)
            {
                //RankA인 상황에서만 플레이어가 움직였을때 다시 돌아온다
                if(playerRank == PlayerRank.RANK_A)
                {
                    talkBubble.SetActive(false);
                    talkText.text = "";
                }
            }
        }

        #endregion

        #region StateController

        switch (playerRank)
        {
            case PlayerRank.RANK_A:

                break;
            case PlayerRank.RANK_B:

                rankBpauseTime += Time.deltaTime;

                if(rankBpauseTime >= rankBTIme)
                {
                    int bRandom = Random.Range(0, 10);

                    if(bRandom <= stopChance)
                    {
                        //확률이 되면 캐릭터를 일정시간 멈추는 코루틴을 호출한다. 
                        StartCoroutine(this.RankBTalkBubble());
                        StartCoroutine(charaControll.RankBStopMotion());
                    }

                    rankBpauseTime = 0.0f;
                }

                if(!rankChange)
                {
                    StartCoroutine(this.ChangeRank());
                    rankChange = true;
                }

                break;

            case PlayerRank.RANK_C:

                //RankC 에서는 조작을 반대로 실행함
                isAngry = true;

                rankBpauseTime += Time.deltaTime;

                if(rankBpauseTime >= 4.0f)
                {
                    //RankC는 대사만 주기적으로 출력해준다
                    StartCoroutine(this.RankBTalkBubble());

                    rankBpauseTime = 0.0f;
                }

                if(rankChange)
                {
                    StartCoroutine(this.ChangeRank());
                    rankChange = false;
                }

                break;

            case PlayerRank.RANK_D:

                isAngry = false;
                isResign = true;

                findTime += Time.deltaTime;

                if(findTime <= 1.5f)
                {
                    //Rank D 에 들어오면 DeadZoneTarget 을 잡는다
                    deadZoneTarget = FindDeadZone();
                    charaControll.GetResign();
                    StartCoroutine(this.RankBTalkBubble());
                }
                else if (findTime > 1.5f)
                {
                    //GetResign에서 켜준 StopMotionRankB 꺼준다
                    charaStatus.StopMotionRankB = false;

                    //DeadZone Target 을 따라 이동한다
                    charaControll.FollowDeadZone(deadZoneTarget);

                }

                break;

            default:

                break;
        }

        #endregion
    }

    IEnumerator RankBTalkBubble()
    {
        talkBubble.SetActive(true);

        if (playerRank == PlayerRank.RANK_B)
        {
            int randomTalk = Random.Range(0, BRankText.Length);
            talkText.text = BRankText[randomTalk];

            yield break;
        }
        else if (playerRank == PlayerRank.RANK_C)
        {
            int randomTalk = Random.Range(0, CRankText.Length);
            talkText.text = CRankText[randomTalk];

            anim.SetTrigger("Angry");
        }
        else if (playerRank == PlayerRank.RANK_D)
        {
            talkText.text = DeadText[0];
        }

        yield return new WaitForSeconds(1.5f);

        if (talkBubble.activeSelf == true)
        {
            talkBubble.SetActive(false);
        }
    }

    public void BubbleOffFunc()
    {
        if (talkBubble.activeSelf == true)
        {
            talkBubble.SetActive(false);
        }
    }

    private Transform FindDeadZone()
    {
        GameObject[] deadZones;
        deadZones = GameObject.FindGameObjectsWithTag(deadZoneTag);

        Transform target = null;
        float dist = Mathf.Infinity;
        Vector2 position = transform.position;

        if (deadZones != null)
        {
            foreach (GameObject dead in deadZones)
            {
                Vector2 diff = (Vector2)dead.transform.position - position;
                float curDistance = diff.sqrMagnitude;
        
                if (curDistance < dist)
                {
                    target = dead.transform;
                    dist = curDistance;
                }
            }
        }
        else
        {
            Debug.Log("Near Spike is NULL !! : EmotionController");
        }

        return target;
    }

    IEnumerator ChangeRank()
    {
        talkBubble.SetActive(true);

        if(playerRank == PlayerRank.RANK_B)
        {
            int randomTalk = Random.Range(0, BRankText.Length);
            talkText.text = BRankText[randomTalk];
        }
        else if (playerRank == PlayerRank.RANK_C)
        {
            int randomTalk = Random.Range(0, CRankText.Length);
            talkText.text = CRankText[randomTalk];
        }

        yield return new WaitForSeconds(1.5f);

        talkBubble.SetActive(false);
    }
}
