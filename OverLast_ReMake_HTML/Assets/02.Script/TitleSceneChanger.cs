﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleSceneChanger : MonoBehaviour
{
    public enum ChooseDevice
    {
        Device_PC,
        Device_Mobile
    }

    public Text debugTextField = null;
    public Text textPC;
    public Text textMobile;
    public GameObject pcBtn, mobileBtn;
    public Image coinImg;
    public Color _color = new Color(0, 0, 0, 0);

    private bool choosePc = false;
    private bool chooseMobile = false;
    private float textBlinkTime;
    private ChooseDevice selectDevice;

    public float TextBlinkTime { get { return textBlinkTime; } }

    //첫 Title 화면에서 엔딩 유/무에따라 씬 바꾸어줌
    public void ChangeScene()
    {
        if(PlayerPrefs.HasKey("Ending"))
        {
            SceneManager.LoadScene(2);
        }
        else
        {
            SceneManager.LoadScene(1);
        }
    }

    private void Start()
    {
        if(!mobileBtn.activeSelf)
        {
            mobileBtn.SetActive(true);
        }
        if(!pcBtn.activeSelf)
        {
            pcBtn.SetActive(true);
        }

        textBlinkTime = 0f;

        selectDevice = ChooseDevice.Device_Mobile;
    }

    public void Update()
    {
        if(choosePc)
        {
            textBlinkTime += Time.deltaTime;

            if(textBlinkTime > 0f && textBlinkTime <= 0.3f)
            {
                textPC.enabled = true;
            }
            else if(textBlinkTime > 0.3f && textBlinkTime <= 0.6f)
            {
                textPC.enabled = false;
            }
            else if (textBlinkTime > 0.6f)
            {
                textBlinkTime = 0f;
            }
        }

        if (chooseMobile)
        {
            textBlinkTime += Time.deltaTime;

            if (textBlinkTime > 0f && textBlinkTime <= 0.3f)
            {
                textMobile.enabled = true;
            }
            else if (textBlinkTime > 0.3f && textBlinkTime <= 0.6f)
            {
                textMobile.enabled = false;
            }
            else if (textBlinkTime > 0.6f)
            {
                textBlinkTime = 0f;
            }
        }

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if(!choosePc && !chooseMobile)
            {
                selectDevice = ChooseDevice.Device_Mobile;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(!choosePc && !chooseMobile)
            {
                selectDevice = ChooseDevice.Device_PC;
            }
        }

        switch (selectDevice)
        {
            case ChooseDevice.Device_PC:
                coinImg.rectTransform.localPosition = new Vector2(57f, 0f);
                ChangeTextColor(textPC, textMobile);

                //게임을 시작하는 코루틴 실행(PC
                if(Input.GetKeyDown(KeyCode.Space))
                {
                    StartCoroutine(StartGame(0));
                }

                break;
            case ChooseDevice.Device_Mobile:
                coinImg.rectTransform.localPosition = new Vector2(-225f, 0f);
                ChangeTextColor(textMobile, textPC);

                //게임을 시작하는 코루틴 실행(Mobile   
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    StartCoroutine(StartGame(1));
                }

                break;
            default:
                break;
        }
    }

    public void OnPcBtnDown(int number)
    {
        StartCoroutine(StartGame(number));
    }

    IEnumerator StartGame(int num)
    {
        //이미 유저가 결정을 한 상황이라면 Return 해줍시다
        if (!chooseMobile && !choosePc)
            yield return null;

        if(num == 0)
        {
            selectDevice = ChooseDevice.Device_PC;
            choosePc = true;
            //현재 기기를 저장
            PlayerPrefs.SetInt("Device", 0);
        }
        else if(num ==1)
        {
            selectDevice = ChooseDevice.Device_Mobile;
            chooseMobile = true;
            PlayerPrefs.SetInt("Device", 1);
        }

        yield return new WaitForSeconds(2.0f);

        if (PlayerPrefs.HasKey("Ending"))
        {
            SceneManager.LoadScene(2);
        }
        else
        {
            SceneManager.LoadScene(1);
        }
    }

    public void OnDevMenuBtnDown(GameObject DevPanel)
    {
        DevPanel.SetActive(true);
    }

    public void OnDevPanelDown(GameObject DevPanel)
    {
        DevPanel.SetActive(false);
    }

    private void ChangeTextColor(Text textex1, Text textex2)
    {
        textex1.color = _color;
        textex2.color = new Color(255, 255, 255);
    }
}
