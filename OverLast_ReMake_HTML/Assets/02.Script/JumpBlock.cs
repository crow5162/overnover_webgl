﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBlock : MonoBehaviour
{
    public float jumpBlockPower;
    private const string PlayerTag = "Player";
    private AudioSource _audio;

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == PlayerTag)
        {
            var playerRbody = coll.gameObject.GetComponent<Rigidbody2D>();
            var playerJump = coll.gameObject.GetComponent<Animator>().GetBool("isJump");

            if(playerJump)
            {
                //캐릭터가 점프 중인 상황이라면 점프효과 
                playerRbody.AddForce(Vector2.up * jumpBlockPower, ForceMode2D.Impulse);
                //사운드 재생
                _audio.Play();
            }
        }
    }
}
