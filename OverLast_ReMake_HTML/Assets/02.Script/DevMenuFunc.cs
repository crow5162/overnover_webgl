﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DevMenuFunc : MonoBehaviour
{
    public void SaveDataClear()
    {
        //PlayerPrefs에 존재하는 모든 데이터 삭제
        PlayerPrefs.DeleteAll();
    }

    public void On3DSceneBtnDown()
    {
        SceneManager.LoadScene(2);
    }

    public void OnRespawnBtnDown()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            var charaStatus = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterStatus>();
            charaStatus.CharacterRespawn();
        }
    }
}
