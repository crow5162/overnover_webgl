﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct PlayerSfx
{
    public AudioClip jumpSfx;
    public AudioClip deadSfx;
    public AudioClip coinSfx;
    public AudioClip keySfx;
}

public class CharacterControll : MonoBehaviour
{
    public enum MoveDirection
    {
        NONE,
        MOVE_LEFT,
        MOVE_RIGHT
    }

    [Header("Character Moving")]
    public MoveDirection moveDirec;
    private bool isMove = false;
    public float maxSpeed = 3.0f;
    private Rigidbody2D rbody;
    private Animator anim;
    private SpriteRenderer sprite;
    private CharacterEmotionController emotion;

    private bool isJump;
    private float jumpTimeCounter;
    private float jumpTime = 0.3f;

    [Header("Character Jump")]
    public float jumpPower;
    public Transform leftCheckTr;
    public Transform rightCheckTr;

    [Header("Character Ladder")]
    public float ladderSpeed = 3.0f;
    [SerializeField]
    private bool isClimbing = false;
    public float ladderDist = 3.0f;
    private float gravity;

    [Header("Character Die")]
    public CharacterStatus charaStatus;
    private float trackingDist = 0.3f;

    [Header("Mobile Input")]
    [SerializeField] private bool jumpBtnActive = false;
    public bool JumpBtnActive { get { return jumpBtnActive; } set { jumpBtnActive = value; } }

    private readonly int hashClimb = Animator.StringToHash("isClimb");
    private readonly int hashMove = Animator.StringToHash("isMove");
    private readonly int hashJump = Animator.StringToHash("isJump");
    private AudioSource _audio;

    public PlayerSfx playerSfx;

    private void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        emotion = GetComponent<CharacterEmotionController>();
        _audio = GetComponent<AudioSource>();

        if (emotion == null)
        {
            Debug.Log("Character Emotion Controller 가 NULL 입니다 !");
        }

        gravity = rbody.gravityScale;

        CharacterStatus.OnInitChara += InitialCharacter;
    }

    private void Update()
    {
        if (charaStatus.IsDie || charaStatus.StopMotionRankB)
            return;

        //Character Jump (Space Bar) <기존 점프 로직>
        //if(Input.GetKeyDown(KeyCode.Space) && !anim.GetBool("isJump") && !isClimbing)
        //{
        //    if(!emotion.IsResign)
        //    {
        //        rbody.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        //        anim.SetBool("isJump", true);
        //
        //        var jumpSfx = playerSfx.jumpSfx;
        //        _audio.PlayOneShot(jumpSfx, 1.0f);
        //    }
        //}

        // < 키입력 시간에 따른 점프 높이 변화 >
        if (Input.GetKeyDown(KeyCode.Space) && !anim.GetBool("isJump") && !isClimbing)
        {
            if (!emotion.IsResign)
            {
                isJump = true;
                jumpTimeCounter = jumpTime;
                rbody.velocity = new Vector2(rbody.velocity.x, jumpPower);
                anim.SetBool("isJump", true);

                var jumpSfx = playerSfx.jumpSfx;
                _audio.PlayOneShot(jumpSfx, 1.0f);
            }
        }

        if(Input.GetKey(KeyCode.Space) && isJump)
        {
            if(jumpTimeCounter > 0)
            {
                rbody.velocity = new Vector2(rbody.velocity.x, jumpPower);
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJump = false;
            }
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            isJump = false;
        }

        if(isJump && jumpBtnActive)
        {
            if (jumpTimeCounter > 0)
            {
                rbody.velocity = new Vector2(rbody.velocity.x, jumpPower);
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJump = false;
            }
        }

        //KeyControll (Left, Right)
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if(!emotion.IsResign)
            {
                if (!emotion.IsAngry)
                {
                    if (moveDirec != MoveDirection.MOVE_RIGHT && !isClimbing)
                    {
                        anim.SetBool(hashMove, true);
                        moveDirec = MoveDirection.MOVE_LEFT;
                        isMove = true;
                    }
                }
                else
                {
                    if (moveDirec != MoveDirection.MOVE_LEFT && !isClimbing)
                    {
                        anim.SetBool(hashMove, true);
                        moveDirec = MoveDirection.MOVE_RIGHT;
                        isMove = true;
                    }
                }
            }
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if(!emotion.IsResign)
            {
                if (!emotion.IsAngry)
                {
                    if (moveDirec != MoveDirection.MOVE_LEFT && !isClimbing)
                    {
                        anim.SetBool(hashMove, true);
                        moveDirec = MoveDirection.MOVE_RIGHT;
                        isMove = true;
                    }
                }
                else
                {
                    if (moveDirec != MoveDirection.MOVE_RIGHT && !isClimbing)
                    {
                        anim.SetBool(hashMove, true);
                        moveDirec = MoveDirection.MOVE_LEFT;
                        isMove = true;
                    }
                }
            }
        }

        //Stop Speed Set
        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            if(!emotion.IsResign)
            {
                anim.SetBool(hashMove, false);
                //rbody.velocity = new Vector2(rbody.velocity.normalized.x * 0.5f, rbody.velocity.y);
                rbody.velocity = new Vector2(0, rbody.velocity.y);
                moveDirec = MoveDirection.NONE;
                isMove = false;
            }
        }

        #region ImageFlip
        //Sprite Renderer (FlipX Set)
        if(moveDirec == MoveDirection.MOVE_LEFT)
        {
            sprite.flipX = true;
        }
        else if (moveDirec == MoveDirection.MOVE_RIGHT)
        {
            sprite.flipX = false;
        }
        #endregion  

        #region LadderActions
        //Ladder Climbing
        RaycastHit2D hitinfo = Physics2D.Raycast(transform.position, Vector2.up, ladderDist, LayerMask.GetMask("Ladder"));

        if (hitinfo.collider != null)
        {
            //키보드를 이용한 조작
            if(Input.GetKeyDown(KeyCode.UpArrow) && !anim.GetBool("isJump"))
            {
                rbody.velocity = new Vector2(0, 0);
                isClimbing = true;
            }

            //모바일 Input을 이용한 조작
            #region MobileLadder

            //사다리가 감지되었을때만 점프 버튼을 Push 했을때 사다리를 탈 수 있다
            if(jumpBtnActive)
            {
                rbody.velocity = new Vector2(0, 0);
                isClimbing = true;
            }

            #endregion 
        }
        else
        {
            isClimbing = false;

        }

        if(isClimbing)
        {
            //클라이밍 상태일때는 중력 받지않음
            rbody.gravityScale = 0f;

            anim.SetBool(hashClimb, true);
            anim.speed = 0f;

            //KeyBoard를 사용한 조작
            #region KeyBoardInputClimb
            //Climb UP
            if(Input.GetKey(KeyCode.UpArrow))
            {
                rbody.velocity = new Vector2(rbody.velocity.x, 1.0f * ladderSpeed);
                anim.speed = 0.6f;
            }
            else if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                //키를 떼었을 때 무중력으로 밀려나는것을 방지함 
                rbody.velocity = new Vector2(0, 0);
                anim.speed = 0f;
            }

            //Climb Down
            if (Input.GetKey(KeyCode.DownArrow))
            {
                anim.speed = 0.6f;

                //사다리를 탄 상태에서 아래키를 누르고있을때만, 지상으로 착지한 상태인지 체크하여
                //상태를 해제한다
                rbody.velocity = new Vector2(rbody.velocity.x, -1.0f * ladderSpeed);

                RaycastHit2D ladderRay = Physics2D.Raycast(transform.position, Vector3.down, 1.5f, LayerMask.GetMask("Platform"));

                if (ladderRay.collider != null)
                {
                    if (ladderRay.distance < 1.2f)
                    {
                        //클라이밍 상태를 해제함과 동시에 중력 다시적용
                        isClimbing = false;
                        rbody.gravityScale = gravity;
                    }
                }
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                rbody.velocity = new Vector2(0, 0);
                anim.speed = 0f;
            }

            #endregion

            #region MobileInputCLimb

            if(jumpBtnActive)
            {
                rbody.velocity = new Vector2(rbody.velocity.x, 1.0f * ladderSpeed);
                anim.speed = 0.6f;
                anim.SetBool(hashJump, false);
            }
            //키보드를 사용하는 순간에도 항상 체크를 하기 때문에 JumpButtonUp 함수에 넣어두어야 한다
            //else
            //{
            //    //키를 떼었을 때 무중력으로 밀려나는것을 방지함 
            //    rbody.velocity = new Vector2(0, 0);
            //    anim.speed = 0f;
            //}

            #endregion  
        }
        else
        {
            rbody.gravityScale = gravity;
            anim.SetBool(hashClimb, false);
            anim.speed = 1;
        }
        #endregion

        #region Landing Checking

        //Landing Platform (After Jump), Left, Right Checking
        //Not Landing Check In climbing State !!
        if (rbody.velocity.y < 0.5)
        {
            Debug.DrawRay(leftCheckTr.position, Vector3.down, new Color(0, 1, 0));
            RaycastHit2D rayHit = Physics2D.Raycast(leftCheckTr.position, Vector3.down, 1.8f, LayerMask.GetMask("Platform"));

            Debug.DrawRay(rightCheckTr.position, Vector3.down, new Color(0, 1, 0));
            RaycastHit2D rayHitR = Physics2D.Raycast(rightCheckTr.position, Vector3.down, 1.8f, LayerMask.GetMask("Platform"));

            if (rayHit.collider != null || rayHitR.collider != null)
            {
                if (rayHit.distance <= 1.6f || rayHitR.distance <= 1.6f)
                {
                    anim.SetBool("isJump", false);

                    if(charaStatus.IsKnockBack)
                    {
                        rbody.velocity = new Vector2(0, rbody.velocity.y);
                        charaStatus.IsKnockBack = false;
                    }
                }
            }
            else
            {
                anim.SetBool(hashJump, true);
            }
        }

        #endregion  

    }

    private void FixedUpdate()
    {
        if (charaStatus.IsDie || charaStatus.StopMotionRankB)
            return;

        //Move Player to AddForce (Rigid Body)
        if(isMove)
        {
            if(moveDirec == MoveDirection.MOVE_LEFT)
            {
                rbody.AddForce(Vector2.left * 1.0f, ForceMode2D.Impulse);
            }
            else if (moveDirec == MoveDirection.MOVE_RIGHT)
            {
                rbody.AddForce(Vector2.right * 1.0f, ForceMode2D.Impulse);
            }
        }

        //Right Move MaxSpeed
        if(rbody.velocity.x > maxSpeed)
        {
            rbody.velocity = new Vector2(maxSpeed, rbody.velocity.y);
        }
        //Left Move MaxSpeed
        else if (rbody.velocity.x < maxSpeed * (-1))
        {
            rbody.velocity = new Vector2(maxSpeed * (-1), rbody.velocity.y);
        }
    }

    private void OnDisable()
    {
        //Debug.Log("On Disabled !");
        CharacterStatus.OnInitChara -= InitialCharacter;
    }

    private void InitialCharacter()
    {
        //캐릭터 사망시 변수 초기화
        moveDirec = MoveDirection.NONE;
        rbody.velocity = new Vector2(0f, 0f);
        isMove = false;
        isClimbing = false;

        anim.SetBool(hashMove, false);
        anim.SetBool(hashJump, false);
    }

    public void CharacterDieAni()
    {
        anim.SetBool(hashJump, true);
    }

    public IEnumerator RankBStopMotion()
    {
        charaStatus.StopMotionRankB = true;

        if(anim.GetBool(hashJump) == true || anim.GetBool(hashMove) == true)
        {
            isMove = false;
            moveDirec = MoveDirection.NONE;
            rbody.velocity = new Vector2(0f, rbody.velocity.y);
            anim.SetBool(hashJump, false);
            anim.SetBool(hashMove, false);
        }

        yield return new WaitForSeconds(3.0f);

        emotion.BubbleOffFunc();
        charaStatus.StopMotionRankB = false;
    }

    public void GetResign()
    {
        charaStatus.StopMotionRankB = true;

        if (anim.GetBool(hashJump) == true || anim.GetBool(hashMove) == true)
        {
            isMove = false;
            moveDirec = MoveDirection.NONE;
            rbody.velocity = new Vector2(rbody.velocity.normalized.x * 0.5f, rbody.velocity.y);
            anim.SetBool(hashJump, false);
            anim.SetBool(hashMove, false);
        }

    }

    public void FollowDeadZone(Transform target)
    {
        //타겟보다 오른쪽에 위치한 경우
        if (transform.position.x > target.position.x)
        {
            //left Moving
            //가까히 근접했을 경우 좌우로 심하게 요동치는 현상방지 (예외처리
            if ((transform.position.x - target.position.x) < trackingDist)
                return;

            anim.SetBool(hashMove, true);
            moveDirec = MoveDirection.MOVE_LEFT;
            isMove = true;
        }
        else if (transform.position.x < target.position.x)
        {
            //Right Moving
            //가까히 근접했을 경우 좌우로 심하게 요동치는 현상방지 (예외처리
            if ((target.position.x - transform.position.x) < trackingDist)
                return;

            anim.SetBool(hashMove, true);
            moveDirec = MoveDirection.MOVE_RIGHT;
            isMove = true;
        }

        if (transform.position.y < target.position.y)
        {
            if (!anim.GetBool(hashJump) && !isClimbing)
            {
                rbody.AddForce(Vector2.up * jumpPower * 2f, ForceMode2D.Impulse);
                anim.SetBool(hashJump, true);
            }
        }
        else
        {
            //anim.SetBool(hashJump, false);
        }


    }

    #region MobileInput

    public void OnLeftBtnDown()
    {
        if(!emotion.IsResign && !charaStatus.StopMotionRankB)
        {
            if (!emotion.IsAngry)
            {
                if (moveDirec != MoveDirection.MOVE_RIGHT && !isClimbing)
                {
                    anim.SetBool(hashMove, true);
                    moveDirec = MoveDirection.MOVE_LEFT;
                    isMove = true;
                }
            }
            else
            {
                if (moveDirec != MoveDirection.MOVE_LEFT && !isClimbing)
                {
                    anim.SetBool(hashMove, true);
                    moveDirec = MoveDirection.MOVE_RIGHT;
                    isMove = true;
                }
            }
        }
    }

    public void OnRightBtnDown()
    {
        if(!emotion.IsResign && !charaStatus.StopMotionRankB)
        {
            if (!emotion.IsAngry)
            {
                if (moveDirec != MoveDirection.MOVE_LEFT && !isClimbing)
                {
                    anim.SetBool(hashMove, true);
                    moveDirec = MoveDirection.MOVE_RIGHT;
                    isMove = true;
                }
            }
            else
            {
                if (moveDirec != MoveDirection.MOVE_RIGHT && !isClimbing)
                {
                    anim.SetBool(hashMove, true);
                    moveDirec = MoveDirection.MOVE_LEFT;
                    isMove = true;
                }
            }

        }
    }

    public void OnBtnUp()
    {
        if(!emotion.IsResign && !charaStatus.StopMotionRankB)
        {
            anim.SetBool(hashMove, false);
            rbody.velocity = new Vector2(0, rbody.velocity.y);
            moveDirec = MoveDirection.NONE;
            isMove = false;
        }
    }

    public void OnJumpBtnDown()
    {
        if(!emotion.IsResign && !charaStatus.StopMotionRankB)
        {
            jumpBtnActive = true;

            //Not in Ladder Action
            if (!anim.GetBool("isJump") && !isClimbing && !isJump)
            {
                isJump = true;
                jumpTimeCounter = jumpTime;
                rbody.velocity = new Vector2(rbody.velocity.x, jumpPower);
                anim.SetBool("isJump", true);

                var jumpSfx = playerSfx.jumpSfx;
                _audio.PlayOneShot(jumpSfx, 1.0f);
            }
        }
    }

    public void OnJumpBtnUp()
    {
        if(!emotion.IsResign && !charaStatus.StopMotionRankB)
        {
            jumpBtnActive = false;
            isJump = false;

            if (isClimbing)
            {
                rbody.velocity = new Vector2(0, 0);
                anim.speed = 0f;
            }
        }
    }

    #endregion

    #region SoundsFunc

    public void DeadSound()
    {
        var dieSound = playerSfx.deadSfx;
        _audio.PlayOneShot(dieSound, 1.0f);
    }

    public void CoinSound()
    {
        var coins = playerSfx.coinSfx;
        _audio.PlayOneShot(coins, 1.0f);
    }

    public void GetKeySound()
    {
        var getKeySound = playerSfx.keySfx;
        _audio.PlayOneShot(getKeySound, 1.0f);
    }

    #endregion  
}
