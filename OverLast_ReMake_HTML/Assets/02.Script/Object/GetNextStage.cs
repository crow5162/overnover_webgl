﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetNextStage : MonoBehaviour
{
    private SpriteRenderer sprite;
    public Sprite[] computerSprite = new Sprite[2];

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            sprite.sprite = computerSprite[1];
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            sprite.sprite = computerSprite[0];
        }
    }
}
