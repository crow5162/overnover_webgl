﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MYGizmo : MonoBehaviour
{
    public float _radius = 0.3f;
    public Color _color = Color.red;

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, _radius);
        Gizmos.color = _color;
    }
}
