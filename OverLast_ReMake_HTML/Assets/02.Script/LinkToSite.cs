﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkToSite : MonoBehaviour
{
    public string Url;

    public void OnMouseCursorEnter()
    {
        //Cursor.SetCursor()
    }

    public void OnMouseCursorExit()
    {
        
    }

    public void OpenLink()
    {
        Application.OpenURL(Url);
    }
}
