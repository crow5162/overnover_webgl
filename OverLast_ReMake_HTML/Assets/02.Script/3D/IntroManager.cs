﻿using MH.Mumbler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroManager : MonoBehaviour
{
    Animator animator;
    [TextArea]
    public string[] TalkText;
    public enum AniState { IDLE, WAVING, THANKFUL, ARMGESTURE }
    public AniState[] aniStates;
    public Text m_text;
    public float textTimer;         // 설정된 시간마다 글자가 나옴.
    IEnumerator enumerator;
    public int curTextNum;          // 현재 문장
    public int curWordNum;          // 현재 단어
    public float[] SoundTime;       // 소리 시간
    public bool isTyping = true;
    public dispersePixels DisPix;
    public SpeakerData[] speakerDatas;
    Coroutine _co;
    public GameObject nextBtn;

    private void Awake()
    {
        DisPix.enabled = true;
    }

    private void Start()
    {
        if (Scene3DManager.instance.gameState == Scene3DManager.GameState.INTRO)
        {
            animator = GetComponent<Animator>();
            enumerator = TextTyping();
            StartCoroutine(enumerator);

            int device = PlayerPrefs.GetInt("Device");

            if(device == 0)
            {
                nextBtn.SetActive(false);
            }
            else if (device == 1)
            {
                nextBtn.SetActive(true);
            }
        }
        else
        {
            this.gameObject.SetActive(false);
        }

        if(DisPix.enabled)
        {
            DisPix.enabled = false;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))             // 스킵 & 다음 대사로 넘어갈때 키 변화시키는 곳 (현재는 Space)
        {
            StopCoroutine(enumerator);
            enumerator = null;
            enumerator = TextTyping();
            curWordNum = 0;
            if (isTyping)
            {
                isTyping = false;
                m_text.text = TalkText[curTextNum];
            }
            else
            {
                curTextNum++;
                if (curTextNum < TalkText.Length)
                {
                    isTyping = true;
                    StartCoroutine(enumerator);
                }
                else
                {
                    DisPix.enabled = true;
                    StartCoroutine(GetStart());
                }
            }
        }
    }

    IEnumerator TextTyping()
    {
        curWordNum = 0;
        m_text.text = "";
        animator.SetInteger("AniNum", (int)aniStates[curTextNum]);
        animator.SetTrigger("Trigger");
        //   SoundManager.Instance.audioSource.PlayOneShot(audioClips[curTextNum]);
        if (_co != null)
            MumbleSpeak.Instance.StopSpeak(_co);
        _co = MumbleSpeak.Instance.Speak(SoundTime[curTextNum], speakerDatas[curTextNum].name, transform);
        yield return null;
        while (isTyping)
        {
            if (curWordNum > TalkText[curTextNum].Length)
            {
                isTyping = false;
            }
            else
            {
                m_text.text = TalkText[curTextNum].Substring(0, curWordNum);
                curWordNum++;
                yield return new WaitForSecondsRealtime(textTimer);
            }
        }
    }

    IEnumerator GetStart()
    {
        nextBtn.SetActive(false);

        yield return new WaitForSeconds(3.0f);

        DisPix.enabled = false;
        this.gameObject.SetActive(false);
        Scene3DManager.instance.gameState = Scene3DManager.GameState.INGAME;
        Scene3DManager.instance.OnEnabledButtons();
        SoundManager.instance.StartBackGroundMusic();
    }

    public void NextScriptButton()
    {
        //Mobile Device Next Script Button Push !
        StopCoroutine(enumerator);
        enumerator = null;
        enumerator = TextTyping();
        curWordNum = 0;
        if (isTyping)
        {
            isTyping = false;
            m_text.text = TalkText[curTextNum];
        }
        else
        {
            curTextNum++;
            if (curTextNum < TalkText.Length)
            {
                isTyping = true;
                StartCoroutine(enumerator);
            }
            else
            {
                DisPix.enabled = true;
                StartCoroutine(GetStart());
            }
        }
    }
}
