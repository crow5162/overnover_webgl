﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LittleHelper : MonoBehaviour
{
    public string HelperText;
    public Image bubbleImage;
    public TextMeshProUGUI scriptText;

    private BoxCollider2D boxColl;

    private void Start()
    {
        boxColl = GetComponent<BoxCollider2D>();

        if (scriptText != null)
        {
            scriptText.enabled = false;
            bubbleImage.enabled = false;
        }
    }

    private void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Player") &&
            GameManager.instance.laps > 2)
        {
            StartCoroutine(this.ShowHelper());
        }
    }

    IEnumerator ShowHelper()
    {
        bubbleImage.enabled = true;
        scriptText.enabled = true;
        scriptText.text = HelperText;

        yield return new WaitForSeconds(1.0f);

        bubbleImage.enabled = false;
        scriptText.enabled = false;
        scriptText.text = "";
    }
}
