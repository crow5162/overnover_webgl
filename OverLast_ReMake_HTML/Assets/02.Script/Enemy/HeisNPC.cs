﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HeisNPC : MonoBehaviour
{
    [Header("NPC Mode")]
    [Tooltip("NPC 대사의 SIZE 수정 금지")]
    [TextArea()] public string[] npcScripts = new string[4];
    public Transform npcModePosition;
    public TextMeshProUGUI scriptsText;
    public Image bubbleImg;

    public enum TalkingState
    {
        NONE,
        TALK_01,
        TALK_02,
        TALK_03,
        TALK_04
    }

    private BoxCollider2D boxColl;
    private EnemyController eControll;
    private SpriteRenderer sprite;
    private bool isTalking = false;
    private float talkingTime = 0.0f;
    private TalkingState talking;


    private void Start()
    {
        boxColl = GetComponent<BoxCollider2D>();
        eControll = GetComponent<EnemyController>();
        sprite = GetComponent<SpriteRenderer>();

        if(scriptsText != null)
        {
            scriptsText.enabled = false;
            bubbleImg.enabled = false;
        }
    }

    private void Update()
    {
        #region LapsChecking

        if(GameManager.instance.laps > 2)
        {
            if(eControll.isActiveAndEnabled)
            {
                //2바퀴 이상일 때 본 NPC를 컨트롤 하던 스크립트를 꺼준다
                eControll.enabled = false;
                //방향 맞추기
                sprite.flipX = false;
            }

            //돌아다니지 않고 한 자리에 머무르며 플레이어가 다가오기를 기다린다
            transform.position = npcModePosition.position;

            //플레이어와 충돌 하지 않도록 Trigger 켜주고, 콜라이더 사이즈를 증가시켜 더욱 잘 인식되도록 한다.
            boxColl.isTrigger = true;
            boxColl.size = new Vector2(3f, 3f);
        }

        #endregion

        #region StartTalking

        if(isTalking)
        {
            //플레이어와 마주치게 되면 시간마다 대사를 출력합니다
            talkingTime += Time.deltaTime;

            if (talkingTime <= 3.0f)
            {
                talking = TalkingState.TALK_01;
                bubbleImg.enabled = true;
                scriptsText.enabled = true;
            }
            else if (talkingTime > 3.0f && talkingTime <= 6.0f)
            {
                talking = TalkingState.TALK_02;
            }
            else if (talkingTime > 6.0f && talkingTime <= 9.0f)
            {
                talking = TalkingState.TALK_03;
            }
            else if (talkingTime > 9.0f && talkingTime <= 12.0f)
            {
                talking = TalkingState.TALK_04;
            }
            else if (talkingTime > 12f)
            {
                //모든 대사가 끝나면 대화를 종료합니다
                talking = TalkingState.NONE;
                bubbleImg.enabled = false;
                scriptsText.enabled = false;
                isTalking = false;
                talkingTime = 0f;
            }
        }

        #endregion

        #region TalkState

        switch(talking)
        {
            case TalkingState.TALK_01:
                scriptsText.text = npcScripts[0];
                break;
            case TalkingState.TALK_02:
                scriptsText.text = npcScripts[1];
                break;
            case TalkingState.TALK_03:
                scriptsText.text = npcScripts[2];
                break;
            case TalkingState.TALK_04:
                scriptsText.text = npcScripts[3];
                break;
            case TalkingState.NONE:
                break;
        }

        #endregion

    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            if(!isTalking && GameManager.instance.laps > 2)
            {
                //키를 가지고 있지 않은 상황에서만 대화 시작함
                if(!GameManager.instance.IsKey)
                {
                    isTalking = true;
                }
            }
        }
    }

}
