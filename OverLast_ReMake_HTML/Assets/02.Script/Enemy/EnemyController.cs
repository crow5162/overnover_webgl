﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float moveSpeed;
    public bool isPatrolling;
    public float curveTime;
    public float animSpeed = 0.3f;
    public int decreaseScoreCount = 5;
    public LittleHelper littleHelper;

    private float curveT = 0.0f;
    private float knockBackForce = 15.0f;
    private Animator anim;
    private SpriteRenderer sprite;
    private CharacterStatus pState;
    private CharacterEmotionController emotion;
    private BoxCollider2D boxColl;
    private bool helper = false;
    private AudioSource _audio;

    private const string playerTag = "Player";

    public enum EnemyDireciton
    {
        MOVE_LEFT,
        MOVE_RIGHT
    }

    public EnemyDireciton eDirec;

    private void Start()
    {
        //게임이 시작되고 한번만 캐싱해둡니다. (플레이어 GameObject)
        pState = GameObject.FindGameObjectWithTag(playerTag).GetComponent<CharacterStatus>();
        emotion = pState.gameObject.GetComponent<CharacterEmotionController>();
        if(pState == null)
        {
            Debug.Log("Player State 캐싱 실패 ! :: Player Tag를 가진 오브젝트를 찾을 수 없음.");
        }

        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        boxColl = GetComponent<BoxCollider2D>();

        //애니메이션 속도 조정
        anim.speed = animSpeed;

        //자신이 Helper 인지 확인
        if (littleHelper != null)
        {
            helper = true;
        }

        _audio = GetComponent<AudioSource>();

    }

    private void Update()
    {
        if(isPatrolling)
        {
            PatrolingFunc();
        }

        if(emotion.IsResign)
        {
            boxColl.isTrigger = true;
        }
        else if(!emotion.IsResign && !helper)
        {
            boxColl.isTrigger = false;
        }
    }

    private void PatrolingFunc()
    {
        //몬스터의 정찰 기능
        curveT += Time.deltaTime;

        if(eDirec == EnemyDireciton.MOVE_LEFT)
        {
            sprite.flipX = false;
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);

            if(curveT >= curveTime)
            {
                curveT = 0.0f;
                eDirec = EnemyDireciton.MOVE_RIGHT;
            }
        }
        else if (eDirec == EnemyDireciton.MOVE_RIGHT)
        {
            sprite.flipX = true;
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);

            if (curveT >= curveTime)
            {
                curveT = 0.0f;
                eDirec = EnemyDireciton.MOVE_LEFT;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == playerTag)
        {
            if(!helper)
            {
                var pRbody = coll.gameObject.GetComponent<Rigidbody2D>();
                var pTransform = coll.gameObject.GetComponent<Transform>();

                if (eDirec == EnemyDireciton.MOVE_LEFT)
                {
                    //몬스터가 왼쪽으로 이동하고있다면 플레이어 캐릭터와 충돌시 왼쪽으로 밀어냄
                    pState.CollisionWithEnemy(0);
                }
                else if (eDirec == EnemyDireciton.MOVE_RIGHT)
                {
                    //오른쪽으로 밀어냄
                    pState.CollisionWithEnemy(1);
                }

                GameManager.instance.DecreaseScore(decreaseScoreCount);
                GameManager.instance.CheckPlayerRank();

                //Sound 재생
                _audio.Play();
            }
            else if (helper)
            {
                //히든 요소가 숨겨진 상황
                if (GameManager.instance.laps <= 2)
                {
                    var playerRbody = coll.gameObject.GetComponent<Rigidbody2D>();
                    var playerTr = coll.gameObject.GetComponent<Transform>();

                    if (eDirec == EnemyDireciton.MOVE_LEFT)
                    {
                        //몬스터가 왼쪽으로 이동하고있다면 플레이어 캐릭터와 충돌시 왼쪽으로 밀어냄
                        //pState.CollisionWithEnemy(0);
                        playerRbody.AddForce(new Vector2(playerTr.position.x - knockBackForce,
                                                         playerTr.position.y + knockBackForce));
                    }
                    else if (eDirec == EnemyDireciton.MOVE_RIGHT)
                    {
                        //오른쪽으로 밀어냄
                        //pState.CollisionWithEnemy(1);
                        playerRbody.AddForce(new Vector2(playerTr.position.x + knockBackForce,
                                                         playerTr.position.y + knockBackForce));
                    }

                    GameManager.instance.DecreaseScore(decreaseScoreCount);
                    GameManager.instance.CheckPlayerRank();

                    //Sound 재생
                    _audio.Play();
                }
                //히든 요소가 열린 상황 ( 2바퀴 이상 )
                else if (GameManager.instance.laps > 2)
                {
                    var playerRigidBody = coll.gameObject.GetComponent<Rigidbody2D>();
                    var playerJumpState = coll.gameObject.GetComponent<Animator>().GetBool("isJump");

                    if(playerJumpState)
                    {
                        playerRigidBody.AddForce(Vector2.up * 50.0f, ForceMode2D.Impulse);
                    }
                    else
                    {
                        boxColl.isTrigger = true;
                    }
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag("Player"))
        {
            var playerRigidBody = coll.gameObject.GetComponent<Rigidbody2D>();
            var playerJumpState = coll.gameObject.GetComponent<Animator>().GetBool("isJump");

            if(playerJumpState)
            {
                playerRigidBody.AddForce(Vector2.up * 65.0f, ForceMode2D.Impulse);
                boxColl.isTrigger = false;
            }
        }
    }

}
