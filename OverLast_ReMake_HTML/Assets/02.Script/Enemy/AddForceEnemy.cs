﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForceEnemy : MonoBehaviour
{
    public float moveSpeed;
    public bool isPatrolling;
    public float curveTime;
    public float animSpeed = 0.3f;
    public int decreaseScoreCount = 5;
    public float knockBackPower = 15.0f;

    private float curveT = 0.0f;
    private Animator anim;
    private SpriteRenderer sprite;
    private BoxCollider2D boxColl;
    private AudioSource _audio;

    private const string playerTag = "Player";

    public enum EnemyDireciton
    {
        MOVE_LEFT,
        MOVE_RIGHT
    }

    public EnemyDireciton eDirec;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        boxColl = GetComponent<BoxCollider2D>();

        //애니메이션 속도 조정
        anim.speed = animSpeed;

        _audio = GetComponent<AudioSource>();

    }

    private void Update()
    {
        if (isPatrolling)
        {
            PatrolingFunc();
        }
    }

    private void PatrolingFunc()
    {
        //몬스터의 정찰 기능
        curveT += Time.deltaTime;

        if (eDirec == EnemyDireciton.MOVE_LEFT)
        {
            sprite.flipX = false;
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);

            if (curveT >= curveTime)
            {
                curveT = 0.0f;
                eDirec = EnemyDireciton.MOVE_RIGHT;
            }
        }
        else if (eDirec == EnemyDireciton.MOVE_RIGHT)
        {
            sprite.flipX = true;
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime);

            if (curveT >= curveTime)
            {
                curveT = 0.0f;
                eDirec = EnemyDireciton.MOVE_LEFT;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == playerTag)
        {
            var playerRBody = coll.gameObject.GetComponent<Rigidbody2D>();
            var playerTr = coll.gameObject.transform.position;

            if (eDirec == EnemyDireciton.MOVE_LEFT)
            {
                //몬스터가 왼쪽으로 이동하고있다면 플레이어 캐릭터와 충돌시 왼쪽으로 밀어냄
                playerRBody.AddForce(new Vector2(playerTr.x - knockBackPower,
                                                 playerTr.y + knockBackPower), ForceMode2D.Impulse);
            }
            else if (eDirec == EnemyDireciton.MOVE_RIGHT)
            {
                //오른쪽으로 밀어냄
                playerRBody.AddForce(new Vector2(playerTr.x + knockBackPower,
                                                 playerTr.y + knockBackPower), ForceMode2D.Impulse);
            }

            GameManager.instance.DecreaseScore(decreaseScoreCount);
            GameManager.instance.CheckPlayerRank();

            //Sound 재생
            _audio.Play();
        }
    }
}
