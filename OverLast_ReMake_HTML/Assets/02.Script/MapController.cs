﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    private const string PlayerTag = "Player";
    public bool isMapA;
    private bool hiddenObjShow = false;
    public GameObject mapHiddingObj;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == PlayerTag)
        {
            if (GameManager.instance.laps >= 2)
            {
                hiddenObjShow = true;
            }

            if (isMapA)
            {
                GameManager.instance.MapBControll(true, hiddenObjShow);

                if(GameManager.instance.laps >= 2)
                {
                    GameManager.instance.MapBMove();
                }
            }
            else
            {
                GameManager.instance.MapAController(true, hiddenObjShow);

                if (GameManager.instance.laps >= 2)
                {
                    GameManager.instance.MapAMove();
                }
            }

            GameManager.instance.CoinsRespawn();

            mapHiddingObj.SetActive(true);
        }
    }
}
