﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PortalSfx
{
    public AudioClip[] portalSound;
}

public class Portal : MonoBehaviour
{
    public enum PortalType
    {
        Warphole,
        Door,
        Ladder
    }
    public PortalType portalType;

    public Transform nextPoint;
    private const string PlayerTag = "Player";
    private AudioSource _audio;
    public PortalSfx portalSfx;

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == PlayerTag)
        {
            if(portalType == PortalType.Warphole)
            {
                _audio.PlayOneShot(portalSfx.portalSound[0], 1.0f);
                coll.transform.position = nextPoint.position;
            }
            if(portalType == PortalType.Door || portalType == PortalType.Ladder)
            {
                GameManager.instance.UpBtnGuideOn(true);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject.tag == PlayerTag)
        {
            if (portalType == PortalType.Door)
            {
                GameManager.instance.OnActionBtnActive(true);

                if (Input.GetKey(KeyCode.UpArrow) || GameManager.instance.actionBtnOn)
                {
                    _audio.PlayOneShot(portalSfx.portalSound[1], 1.0f);
                    coll.transform.position = nextPoint.position;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        GameManager.instance.UpBtnGuideOn(false);
        GameManager.instance.OnActionBtnActive(false);
    }
}
